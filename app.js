var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var setupPassport = require('./setupPassport');
var routes = require('./routes');
var morgan = require('morgan');
var app = express();

mongoose.connect('mongodb://root:root@ds119578.mlab.com:19578/getting_started');

app.use(morgan('short'));

setupPassport();

app.set('port', process.env.PORT || 3000);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
	secret: 'F:!@(#*!)@#!@#SDASLD_@)+)',
	resave: true,
	saveUninitialized: true
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.use(routes);

app.listen(app.get('port'), function() {
	console.log('Server started on port', app.get('port'));
})